const express=require('express');
const Controller=require('../controller');
const controller = require('../controller');
const router=express.Router();
router.post('/create',Controller.UserController.postuser);
router.post('/login',Controller.UserController.getlogin)
router.post('/accessed',Controller.UserController.verifyToken,Controller.UserController.verify)
router.post('/sendmsg',Controller.UserController.sendmsg);
router.get('/getallchats',controller.UserController.getallchats);
router.get('/getprevchats/:RoomId',Controller.UserController.getprevchats)
module.exports=router;

