const express=require('express');
const mongoose=require('mongoose');
const mongodb=require('mongodb');
const connection=require('../../../common/connection')
const jwt=require('jsonwebtoken')
const {io}=require('../../sockets')
const Users=require('../../../models/User_Sch_Mod')
const SendMsg=require('../../../models/SendMsg_Sch_Mod')
const AllMsg=require('../../../models/AllMsg_Sch_mod')
module.exports.postuser=async(req,res)=>{
    try{
        let data=await Users.create({
            fname:req.body.fname,
            lname:req.body.lname,
            phone:req.body.phone
        });
        // io.to(req._id).emit('usercreated',{message:'user created',data:data});
        return res.status(200).json({
            message:"created successfully!!",
            data:data
        }); 

        
    }catch(error){
        return res.status(400).json({
            message:"creation unsuccessfull!",
            error:error
        });
    }
}

const secretKey="secretKey"
module.exports.getlogin=async(req,res)=>{
    console.log("hi");
    let fname=req.body.fname
    let phone=req.body.phone
    console.log(fname);
    console.log(phone);
    let checkuser=await Users.findOne({phone:phone});
    console.log(checkuser);
    if(checkuser){
        let checkId=checkuser._id;
        let jti=Math.random();
        console.log('jti',jti);
        let jtiUpdate=await Users.updateOne({_id:checkId},
            {$set:{jti:jti}});
            console.log(jtiUpdate);
        jwt.sign({checkId:checkId,jti:jti},secretKey,{expiresIn:'1h'},(err,token)=>{
            if(err){
                console.log(err);
            }
            else{
                console.log(token);
                return res.json({token:token})
            }
        })
    }
}

module.exports.verify=async(req,res)=>{
    jwt.verify(req.token,secretKey,async(err,authData)=>{
        console.log(authData)
        if(err){
            res.send({result:"invalid token"});
        }
        else{
            console.log(req);
            console.log(authData);
            const jtiId=authData.jti;
            let data=await Users.findOne({_id:authData.checkId},{jti:jtiId});
            console.log(data);


            if(data){
                res.send("data fetched");
                req.userId=authData.checkId;
                next();
            }
            else{
                return res.send("data doesn't exist!!");
            }
        }
    })
}

module.exports.verifyToken=async(req,res,next)=>{
    const bearerHeader=req.headers['authorization'];
    console.log(bearerHeader);
    if(typeof bearerHeader!==undefined){
        const bearer=bearerHeader.split(" ");
        console.log("b",bearer)
        const token=bearer[1];
        console.log("t",token)
        req.token=token;
        console.log(req.token)
        next();
    }
}


// sendmsg post api
module.exports.sendmsg=async(req,res)=>{
    try{
        let data=await SendMsg.create(req.body);
        return res.status(200).json({
            msg:"created successfully!!",
            data:data
        })
    }catch(error){
        return res.json({
            msg:"error",
            error:error
        })
    }
}


// get all chats
module.exports.getallchats=async(req,res)=>{
    try{
        let result=await AllMsg.find({});
        console.log(result);
        return res.json({
            msg:"fetching successful!!",
            data:result
        })
    }
    catch(error){
        return res.json({
            msg:"failed to fetch details!!"
        })
    }
}


module.exports.getprevchats=async(req,res)=>{
    let roomId=req.params.RoomId;
    console.log(roomId);
    let newid=new mongodb.ObjectId(roomId);
    console.log(newid)
    try {
        let result=await AllMsg.findOne({RoomId:newid}).sort({"createdAt":-1})
    console.log(result);
    return res.json({
        msg:"sucessful",
        data:result
    })
        
    } catch (error) {
        return res.json({
            msg:"fetching unsuccessful!!",
            error:error
        })
    }
}