const express = require('express');
const http = require('http');
const { Server } = require("socket.io");
const mongoose = require('mongoose');
const connection = require('../../common/connection');

const Users=require('../../models/User_Sch_Mod')
const SendMsg = require('../../models/SendMsg_Sch_Mod');
const AllMsg=require('../../models/AllMsg_Sch_mod')

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server,{
    cors:{
        origin:"http://localhost:5174"
    }
})


app.use(express.json()); 

// Socket.IO connection handler
io.on('connection', (socket) => {
    console.log('A user connected');

//to authenticate userid1 and userid2 and roomid and join room
socket.on("join-room",async(data)=>{
       try{ console.log("hii");
            console.log(data);
        // console.log(data._id);
        let userid1=data.userId1;
        let userid2=data.userId2;
        console.log(userid1,userid2)
        // to check whether id exists in db or not
        let checkId=await SendMsg.findOne({userId1:userid1},{userId2:userid2})
        console.log("res",checkId);
        // let roomId=checkId._id
        if(checkId===null){
            console.log("creating details")
            let result=await SendMsg.create({
                userId1:userid1,
                userId2:userid2,
            })
            socket.emit('connectroom',result)
        }
        else{
            
            socket.join(checkId._id);
            console.log(checkId._id ,"room joined by 2 users")
            console.log("user joined")
            socket.emit("connectroom","connected")
        }
        }catch(error){
            socket.emit('connectroom',"error");
        }
    })

// to send msg thorugh that room(msg and roomid)
socket.on('sendmsg',async(data)=>{
    console.log(data);
    let roomId=data._id;
    console.log(roomId)
    let result=await SendMsg.findOne({_id:roomId});
    console.log("result",result);
    if(result){
        console.log(data.message);
        let existingdata=await AllMsg.create({
            RoomId:data._id,
            RoomMsg:data.message,
            userId1:data.userId1,
            userId2:data.userId2

        });
        console.log("result",existingdata);
        // socket.to(roomId).emit('receive-msg',{msg:data.message,msg2:result});
    }
})


// leave the room 
socket.on('leave-room',async(data)=>{
    try{
        let roomId=data._id
        console.log(roomId);
        console.log(data);
        let result= await SendMsg.findOne({_id:roomId});
        console.log("result",result);
        if(result._id)
     {   socket.leave(roomId);
        console.log('user has left the room',data.userId)
    socket.to(roomId).emit('left-room',data.userId)}        
    }catch(error){
        socket.emit('left-room',"error")
    }
})
    // Handle disconnection
    socket.on('disconnect', () => {
        console.log('User disconnected');
    });
});

const PORT = process.env.PORT || 1000;
server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
