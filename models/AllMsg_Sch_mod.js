
const mongoose=require('mongoose');
const AllMsgSch=new mongoose.Schema({
    userId1:{
        type:String
    },
    userId2:{
        type:String
    },
    RoomId:{
        type: mongoose.Types.ObjectId
    },
    RoomMsg:{
        type:String
    }
},
{timestamps:
    true
},
    // {
    //     toJSON:{
    //         transform(doc,ret){
    //             delete ret.password;
    //         }
    //     }
    // }
);


const AllMsg=mongoose.model('allMsg',AllMsgSch);
module.exports=AllMsg;